import WebMidi from 'webmidi'
import { init, updateFromMidiInput } from './x-touch/'

import { getXminiIo } from './x-touch/helpers'

const controllerUpdateCallback = (e) => {
  const { xMiniOut } = getXminiIo(WebMidi)
  if (xMiniOut) {
    if (e.type === 'controlchange') {
      console.log(`should send to controller ${e.address}: ${e.value}`)
      xMiniOut.sendControlChange(e.address, e.value, 10)
    }
    if (e.type === 'Note') {
      // console.log(`should send note ${e.address} with velocity ${e.value}`)
      xMiniOut.playNote(e.address, 10, {
        velocity: e.value,
        rawVelocity: true
      })
    }
  }
}

const updateFunction = (e) => {
  updateFromMidiInput(e)
}

const addListenerOnce = ({ port = null, type = '' }) => {
  if (port && !port.hasListener(type, 'all', updateFunction)) {
    port.addListener(type, 'all', updateFunction)
  }
}

const initXmini = () => {
  const io = getXminiIo(WebMidi)
  if (io.xMiniIn) {
    addListenerOnce({ port: io.xMiniIn, type: 'controlchange' })
    addListenerOnce({ port: io.xMiniIn, type: 'noteon' })
    addListenerOnce({ port: io.xMiniIn, type: 'noteoff' })
    init({ callback: controllerUpdateCallback })
  }
}

const start = () => {
  WebMidi.enable((error) => {
    if (error) {
      console.log(error)
    }
    WebMidi.addListener('connected', (e) => {
      const { xMiniIn } = getXminiIo(WebMidi)
      if (e.port === xMiniIn) {
        console.log('connected x-mini')
        initXmini()
      }
    })
  })
}

const askForMidiPermission = async () => {
  const result = await navigator.permissions.query({
    name: 'midi'
  })
  return result.state === 'granted'
}

const initializeMidiConnection = () => {
  askForMidiPermission().then((result) => {
    if (result) {
      start()
      return true
    }
    return false
  })
}

initializeMidiConnection()
