import { watch } from '@next-vue/runtime-core'

export const getLayer = (layer) => {
  const layerAliases = [
    ['LayerA', 'A', 0, '0'],
    ['LayerB', 'B', 1, '1']
  ]
  const currentLayer = layerAliases.filter((layerAlias) =>
    layerAlias.includes(layer)
  )[0][0]
  return currentLayer
}

export const watchArray = (array, callback) => {
  array.forEach((arrEntry, index) => {
    watch(
      () => arrEntry,
      (arrEntry) => {
        callback(arrEntry, index)
      },
      { lazy: true, deep: true }
    )
  })
}

export const deepSearch = function(object, key, predicate) {
  if (
    Object.hasOwnProperty.call(key, object) &&
    predicate(object, object[key]) === true
  )
    return object

  for (let i = 0; i < Object.keys(object).length; i++) {
    if (typeof object[Object.keys(object)[i]] === 'object') {
      const o = deepSearch(object[Object.keys(object)[i]], key, predicate)
      if (o != null) return o
    }
  }
  return null
}

export const mapRange = (value, low1, high1, low2, high2) => {
  return Math.floor(low2 + ((high2 - low2) * (value - low1)) / (high1 - low1))
}

export const xMiniFilter = (input) =>
  input.manufacturer === 'BEHRINGER International GmbH' &&
  input.name === 'X-TOUCH MINI'

export const getXminiIo = (WebMidi) => {
  const inputDeviceList = WebMidi.inputs.filter((input) => xMiniFilter(input))
  const outputDeviceList = WebMidi.outputs.filter((output) =>
    xMiniFilter(output)
  )

  if (!inputDeviceList.length > 0 && !outputDeviceList.length > 0) {
    return false
  }
  const xMiniIn = inputDeviceList[0]
  const xMiniOut = outputDeviceList[0]
  return { xMiniIn, xMiniOut }
}

export const constrainToMidiRange = (value) => {
  if (value <= 0) {
    return 0
  }
  if (value >= 127) {
    return 127
  }
  return value
}

export const getKeyByValue = (object, value) => {
  return Object.keys(object).find((key) => object[key] === value)
}

export const exportState = (state) => {
  const exportKnobs = (knobs) => {
    return knobs.reduce((acc, knob) => {
      return [
        ...acc,
        {
          active: knob.active,
          turn: {
            value: knob.turn.value,
            mode: knob.turn.mode,
            title: knob.turn.title,
            description: knob.turn.description
          },
          push: {
            value: knob.push.value,
            title: knob.push.title,
            description: knob.push.description
          }
        }
      ]
    }, [])
  }
  const exportButtons = (buttons) => {
    return buttons.reduce((acc, button) => {
      return [
        ...acc,
        {
          active: button.active,
          state: button.state,
          value: button.value,
          buttonMode: button.buttonMode,
          title: button.title,
          description: button.description
        }
      ]
    }, [])
  }
  const exported = {
    operationMode: { value: state.operationMode.value },
    presetLayer: { value: state.presetLayer.value },
    layerA: {
      knobs: exportKnobs(state.layerA.knobs),
      buttons: exportButtons(state.layerA.buttons)
    },
    layerB: {
      knobs: exportKnobs(state.layerB.knobs),
      buttons: exportButtons(state.layerB.buttons)
    }
  }
  return JSON.stringify(exported)
}

export const deepClone = (o) => {
  const out = Array.isArray(o) ? [] : {}
  for (const key in o) {
    const v = o[key]
    out[key] = typeof v === 'object' && v !== null ? deepClone(v) : v
  }
  return out
}
