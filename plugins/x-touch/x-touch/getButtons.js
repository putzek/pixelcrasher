import { getLayer } from './helpers'

export const getButtons = (layer) => {
  const buttonStates = new Map([
    [0, 'Off'],
    [1, 'On'],
    [2, 'Blink']
  ])
  const buttonModes = new Map([
    [0, 'Momentary'],
    [1, 'Switch']
  ])
  const midiMapping = {
    type: 'Note',
    set: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15],
    LayerA: {
      get: [8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23]
    },
    LayerB: {
      get: [32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47]
    }
  }
  const buttons = midiMapping.set.reduce((acc, val, index) => {
    return [
      ...acc,
      {
        controlType: 'button',
        type: midiMapping.type,
        number: index,
        layer: getLayer(layer),
        buttonStates,
        buttonModes,
        get: midiMapping[getLayer(layer)].get[index],
        set: midiMapping.set[index],
        active: true,
        mode: Math.random() > 0.5 ? 1 : 0,
        value: false
      }
    ]
  }, [])
  return buttons
}
