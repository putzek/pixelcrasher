import { watch } from '@next-vue/runtime-core'
import { getLayer, mapRange } from './helpers'

export const registerKnobWatchers = (state, callback) => {
  ;[...state.layerA.knobs, ...state.layerB.knobs].forEach((knob) => {
    const onCurrentLayer = () =>
      getLayer(state.presetLayer.value) === getLayer(knob.layer)
    watch(
      () => knob.turn.value,
      (value) => {
        if (onCurrentLayer()) {
          const newValue = knob.active ? value : 0
          // eslint-disable-next-line standard/no-callback-literal
          callback({
            type: knob.turn.type,
            address: knob.turn.set,
            value: mapRange(newValue, knob.turn.min, knob.turn.max, 0, 13)
          })
        }
      },
      { lazy: false, deep: false }
    )
    /* watch(
      () => knob.turn.mode,
      (mode) => {
        if (onCurrentLayer()) {
          if (knob.active) {
            console.log(`Update mode: ${knob.turn.mode}`)

            // eslint-disable-next-line standard/no-callback-literal
            callback({
              type: knob.turn.type,
              address: knob.turn.setMode,
              value: mode
            })
          }
        }
      },
      { lazy: false, deep: false }
    ) */
  })
}

export const registerButtonWatchers = (state, callback) => {
  ;[...state.layerA.buttons, ...state.layerB.buttons].forEach((button) => {
    const onCurrentLayer = () =>
      getLayer(state.presetLayer.value) === getLayer(button.layer)
    watch(
      () => button.value,
      (value) => {
        if (onCurrentLayer()) {
          if (button.mode === 0) {
            // eslint-disable-next-line standard/no-callback-literal
            callback({
              type: button.type,
              address: button.set,
              value: button.active ? 2 : 0
            })
          } else {
            // eslint-disable-next-line standard/no-callback-literal
            callback({
              type: button.type,
              address: button.set,
              value: button.active ? +value : 0
            })
          }
        }
      },
      { lazy: false, deep: false }
    )
  })
}
