import { reactive } from '@next-vue/runtime-core'
import { getButtons } from './getButtons'
import { getKnobs } from './getKnobs'
import {
  constrainToMidiRange,
  mapRange,
  deepSearch,
  deepClone,
  exportState
} from './helpers'
import { registerButtonWatchers, registerKnobWatchers } from './watchers'

const merge = require('deepmerge')

let outputCallback = (value) => {
  console.log('called output callback with no handler attached')
}

export const setOutputCallback = (callbackFn) => {
  outputCallback = callbackFn
}
const defaultState = {
  operationMode: {
    type: 'CC',
    modes: new Map([
      [0, 'StandardMode'],
      [1, 'MCMode']
    ]),
    get: 0,
    set: 0,
    value: 0
  },
  presetLayer: {
    type: 'ProgramChange',
    modes: new Map([
      [0, 'LayerA'],
      [1, 'LayerB']
    ]),
    get: 0,
    set: 0,
    value: 0
  },
  layerA: {
    knobs: getKnobs('LayerA'),
    buttons: getButtons('LayerA')
  },
  layerB: {
    knobs: getKnobs('LayerB'),
    buttons: getButtons('LayerB')
  }
}
// eslint-disable-next-line import/no-mutable-exports
export let state = reactive(defaultState)

export const importState = (newState = {}) => {
  const newStateMerged = merge(deepClone(state), newState)
  state = reactive(newStateMerged)
  console.log('Importing state:')
  console.log(state)
  registerKnobWatchers(state, outputCallback)
  registerButtonWatchers(state, outputCallback)
  // console.log(exportState(state));
}

export const updateFromMidiInput = (e) => {
  console.log('received midi event')
  function getAddress(midiEvent) {
    const type = midiEvent.type
    if (type === 'controlchange') {
      return midiEvent.controller.number
    }
    if (type === 'noteon' || type === 'noteoff') {
      return midiEvent.note.number
    }
  }
  const control = deepSearch(state, 'get', (o, v) => {
    const typeMatch =
      o.type === e.type ||
      (o.type === 'Note' && e.type === 'noteon') ||
      (o.type === 'Note' && e.type === 'noteoff')
    return typeMatch && o.get === getAddress(e)
  })
  if (control) {
    if (typeof control.updateWithRelativeValue === 'function') {
      console.log({
        eventValue: e.value,
        controlValue: control.value,
        controlValueNew: control.updateWithRelativeValue(control.value, e.value)
      })
      control.value = control.updateWithRelativeValue(control.value, e.value)
    } else if (control.type === 'Note' && control.controlType === 'button') {
      const newValue = e.type === 'noteon'
      if (control.mode === 0) {
        control.value = newValue
      }
      if (control.mode === 1 && e.type === 'noteoff') {
        control.value = !control.value
      }
    }
  } else {
    console.log(`unknown input ${e}`)
  }
}

const sendTestData = (function() {
  let currentTimeout = false
  return () => {
    if (currentTimeout) {
      return currentTimeout
    }
    currentTimeout = setInterval(() => {
      for (let i = 0; i < state.layerA.knobs.length; i++) {
        state.layerA.knobs[i].turn.value = parseInt(
          Math.random() * state.layerA.knobs[i].turn.max,
          10
        )
      }
      for (let i = 0; i < state.layerA.buttons.length; i++) {
        state.layerA.buttons[i].value = Math.random() > 0.5
      }
    }, 5000)
  }
})()

export const init = ({ callback }) => {
  setOutputCallback(callback)
  importState()
  // sendTestData()
}
