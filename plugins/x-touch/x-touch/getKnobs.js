import { getLayer, constrainToMidiRange } from './helpers'

export const getKnobs = (layer) => {
  const modes = new Map([
    [0, 'Single'],
    [1, 'Pan'],
    [2, 'Fan'],
    [3, 'Spread'],
    [4, 'Trim']
  ])
  const midiMapping = {
    mode: {
      type: 'controlchange',
      set: [1, 2, 3, 4, 5, 6, 7, 8]
    },
    turnValues: {
      type: 'controlchange',
      set: [9, 10, 11, 12, 13, 14, 15, 16]
    },
    LayerA: {
      turn: {
        type: 'controlchange',
        get: [1, 2, 3, 4, 5, 6, 7, 8]
      },
      push: {
        type: 'Note',
        get: [36, 37, 38, 39, 40, 41, 42, 43]
      }
    },
    LayerB: {
      turn: {
        type: 'controlchange',
        get: [11, 12, 13, 14, 15, 16, 17, 18]
      },
      push: {
        type: 'Note',
        get: [24, 25, 26, 27, 28, 29, 30, 31]
      }
    }
  }
  const updateWithRelativeValue = (currentValue, rawValue) => {
    const currentValueAsInt = parseInt(currentValue)
    return constrainToMidiRange(
      rawValue > 100
        ? currentValueAsInt - (127 - rawValue)
        : currentValueAsInt + rawValue
    )
  }
  const knobs = midiMapping[getLayer(layer)].turn.get.reduce(
    (acc, val, index) => {
      return [
        ...acc,
        {
          number: index,
          active: true,
          layer: getLayer(layer),
          turn: {
            type: midiMapping[getLayer(layer)].turn.type,
            mode: 0,
            min: 0,
            max: 100,
            value: 50,
            get: midiMapping[getLayer(layer)].turn.get[index],
            set: midiMapping.turnValues.set[index],
            setMode: midiMapping.mode.set[index],
            modes,
            updateWithRelativeValue,
            title: 'Knob ' + index,
            description: ''
          },
          push: {
            controlType: 'push',
            type: midiMapping[getLayer(layer)].push.type,
            get: midiMapping[getLayer(layer)].push.get[index],
            value: false,
            title: '',
            description: ''
          }
        }
      ]
    },
    []
  )
  return knobs
}
