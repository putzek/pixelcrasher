## GIT Methodology

This are overall recommendations on how to work in this project
https://gitlab.com/putzek/pixelcrasher 

Note: This is by no means a classical Scrum description but the one I think can be useful for this particular project. 
* Basically the pending work will be organized in main "Umbrella issues" that are called Stories.
* A story is described with a simple sentence that describes what is the goal and for whom is intended. 
  * From this story can be split up in "n" issues that are connected as associated Gitlab issues. 
* Those stories stay at the left column of the board called _backlog_. 

## Working in one of the issues

* Once it's decided that the story(ies) are going to be the next targets they are moved to **Next** column. 
* From there each developer can choose the issues that are going to be the target on the next days and move them to **Doing** column. 
* And this issues have their own feature branch: `feature/<issue-number>-your_issue_title`

Story titles can be anything on the line of: 

"As a USER_ROLE I want to XXXX"
Example:

```
"As a developer I want to easily configure Network and MQTT 
in a single configuration file"
```

To do so, once the issue is moved to doing, we will create directly a **Create merge request** from Gitlab UX, and doing it like this this merge request is already marked as WIP (Work in Progress)

    DeveloperX @dev-x created merge request !11 to address this issue

* make a git pull in our working environment and checkout this branch before starting to work. This way the process is automatically signaled to other developers. 
* Once issue goals are implemented and tested, push the changes to this branch, and create a merge request
* click on "Resolve WIP status" to signalize that this updates are not anymore Work in progress but already a finished task that can be merged back into **develop**
* assign the merge request to a second developer so he can review what you did and merge it back. In this case the responsability of Merge or not, is from this second person, and he is responsible of the "Code review" process.
 
Note: When creating a Merge request from an Issue, is possible to signalize from what origin branch it should be made, on mouse over in the v (arrow down) -> Source (branch or tag) 

## Branching

**develop** Branch from where we will initiate Merge requests
**master** is the main stable branch where only non-breaking stuff is merged. 
 
Ideally code will be added exclusively using Merge requests. 
That way new features can be merged first only in developer branch and only after they pass many tests and are considered a stable candidate, are merged to master. 
This implies also, that usually we will not merge developer into master, but merge branch per branch instead, keeping a clear difference of what is productive state and what is development work.

Then using this methodology, to work on any new issue, we will make this branch from master and never from development. Since we will work on this new thing from a stable branch and only at a later stage decide if it's going to be merged on this master branch. 
So in one sentence :

 develop **->** WIP merge request N **->** Work + pushes **->** Close WIP and merge in develop 
 
If we want to keep master separated from develop only with stable additions, then once this new branch works and it's tested in develop, can we also merge it in master branch.

Note: For this to happen is important when Resolving the WIP status not to delete the branch we are going to merge in develop.