# Pixelcrasher [ !WIP! ][![pipeline status](https://gitlab.com/putzek/pixelcrasher/badges/master/pipeline.svg)](https://gitlab.com/putzek/pixelcrasher/commits/master)

Pixelcrasher is a tool for creating animations and light effects / patterns via a gui.

# Quickstart 

## Installation for Frontend

Prerequisites: Node

Run
> npm install

in the main directory


## Contributing
Pull requests are welcome. 
For major changes, please open an issue first to discuss what you would like to change.

* [General project methodology](/git-method.md)
* [markdown helpers for documentation](https://docsify.js.org/#/helpers)
