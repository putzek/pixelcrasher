# Hello fellow dev 👋

## Information about frameworks and tools used

This is just an overview, for a complete list please look at [package.json](https://gitlab.com/putzek/pixelcrasher/blob/develop/package.json) 

|                                                                              | description                                                                                              |
|---                                                                           | ---                                                                                                     |
| [Feathers](http://feathersjs.com)                                            | An open source web framework for building modern real-time applications.                                 |
| [FeathersVuex](https://feathers-vuex.feathers-plus.com/)                     | Integration of FeathersJS, Vue, and Nuxt for the artisan developer                                       |
| [Nuxt](https://nuxtjs.org/)                                                  | Nuxt.js presets all the configuration needed to make your development of a Vue.js application enjoyable. |
| [Vue](https://vuejs.org/)                                                    | JavaScript Framework for building the components                                                         |

## Structural overview

[Glossary](/glossary.md)

[![](https://mermaid.ink/img/eyJjb2RlIjoiZ3JhcGggVERcbiAgUHJvamVjdFtQcm9qZWN0XSAtLT58aGFzfCBDb250cm9sbGVycyhDb250cm9sbGVycy9IYXJkd2FyZSlcbiAgUHJvamVjdCAtLT4gfGhhc3wgUGxheWxpc3RzXG4gIENvbnRyb2xsZXJzIC0tPnxoYXN8IENoYW5uZWxze0NoYW5uZWxzfVxuICBDaGFubmVscyAtLT58b3V0cHV0IHRvIHNlcmlhbHwgRE1YW0RNWF1cbiAgQ2hhbm5lbHMgLS0-fG91dHB1dCB0byBVRFBYfCB1ZHB4W1BpeGVscyAvIHNdXG4gIERldmljZXNbRGV2aWNlcyBkZWZpbmVkIGluIHNvZnR3YXJlXSAtLT58ZmVlZCBvdXRwdXQgdG98IENoYW5uZWxzXG4gIFByZXNldHMgLS0-fGhhdmUgMSBvciBtdWx0aXBsZXwgQW5pbWF0aW9uc1tBbmltYXRpb25zXVxuICBBbmltYXRpb25zIC0tPnxhdHRhY2hlZCB0b3wgRGV2aWNlc1xuICBQbGF5bGlzdHMgLS0-IHxjb25zaXN0IG9mfCBQcmVzZXRzXG5cdFx0IiwibWVybWFpZCI6eyJ0aGVtZSI6ImRlZmF1bHQifSwidXBkYXRlRWRpdG9yIjpmYWxzZX0)](https://mermaid-js.github.io/mermaid-live-editor/#/edit/eyJjb2RlIjoiZ3JhcGggVERcbiAgUHJvamVjdFtQcm9qZWN0XSAtLT58aGFzfCBDb250cm9sbGVycyhDb250cm9sbGVycy9IYXJkd2FyZSlcbiAgUHJvamVjdCAtLT4gfGhhc3wgUGxheWxpc3RzXG4gIENvbnRyb2xsZXJzIC0tPnxoYXN8IENoYW5uZWxze0NoYW5uZWxzfVxuICBDaGFubmVscyAtLT58b3V0cHV0IHRvIHNlcmlhbHwgRE1YW0RNWF1cbiAgQ2hhbm5lbHMgLS0-fG91dHB1dCB0byBVRFBYfCB1ZHB4W1BpeGVscyAvIHNdXG4gIERldmljZXNbRGV2aWNlcyBkZWZpbmVkIGluIHNvZnR3YXJlXSAtLT58ZmVlZCBvdXRwdXQgdG98IENoYW5uZWxzXG4gIFByZXNldHMgLS0-fGhhdmUgMSBvciBtdWx0aXBsZXwgQW5pbWF0aW9uc1tBbmltYXRpb25zXVxuICBBbmltYXRpb25zIC0tPnxhdHRhY2hlZCB0b3wgRGV2aWNlc1xuICBQbGF5bGlzdHMgLS0-IHxjb25zaXN0IG9mfCBQcmVzZXRzXG5cdFx0IiwibWVybWFpZCI6eyJ0aGVtZSI6ImRlZmF1bHQifSwidXBkYXRlRWRpdG9yIjpmYWxzZX0)
```
graph TD
  Project[Project] -->|has| Controllers(Controllers/Hardware)
  Project --> |has| Playlists
  Controllers -->|has| Channels{Channels}
  Channels -->|output to serial| DMX[DMX]
  Channels -->|output to UDPX| udpx[Pixels / s]
  Devices[Devices defined in software] -->|feed output to| Channels
  Presets -->|have 1 or multiple| Animations[Animations]
  Animations -->|attached to| Devices
  Playlists --> |consist of| Presets
```
## Raw overview: What does XY do?

### Controllers

A controller is the representation of a single device (for example an led stripe). 
A controller can potentially also have the ability to control multiple stripes in parallel or have completely different skills.
When a controller registers itself, it also publishes its abilities, so that the software can offer the right functionality and options.

On the page "controllers" you can get an overview of all available controllers.
You can also edit the _skills_ that the controller offers.
Example: For a led controller you can create/edit skills and set the length and pixel density of the corresponding stripes.

### Live view

A representation of all skills registered.

### Presets / Playlists

This is the main part of the application.
Here you can create _presets_ consisting of multiple _animations_ and save them in _playlists_.
You can also change the parameters of the _animations_.

A typical workflow:
* Create new preset 
  * repeat x times: 
    * -> create new animation -> add skill group(s) -> set parameters of the animation 
* Save preset
* add preset to a _playlist_ (or multiple _playlists_) if you want to


## Feathers cli

When you want to add feathers functionality, you can use its cli:

Feathers has a powerful command line interface. Here are a few things it can do:

```
$ npm install -g @feathersjs/cli          # Install Feathers CLI

$ feathers generate service               # Generate a new Service
$ feathers generate hook                  # Generate a new Hook
$ feathers generate model                 # Generate a new Model
$ feathers help                           # Show all commands
```
