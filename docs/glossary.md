## Glossary: project specific terminology

| Term        | explanation                                                                                                                                         |
|---          | ---                                                                                                                                                 |
| controller  | representation of a (hardware) device which controls led's or whatever may be possible in the future                                                |
| skill       | representation of a single device (for example an led stripe). A _controller_ can have multiple _skills_                                            |
| skill group | a skill group consists of _skill(s)_ and additionally contains some parameters/settings for how to handle the contained _skills_                    |
| animation   | An animation consists of _skills_ which are organized in _skill groups_. _Animations_ define animation patterns that can be adjusted via parameters |
| preset      | A preset contains multiple animations. All _animations_ are mixed to generate the final output.                                                     |
| playlist    | A playlist is a list of _presets_. A _preset_ can be added to multiple _playlists_.                                                                  |
| action      | The term 'action' is sometimes used to refer to a single tween + config (used in node backend)                                                      |
