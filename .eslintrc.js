const prettierConfig = require('./.prettierrc.js')
module.exports = {
  root: true,
  env: {
    browser: true,
    "es6": true,
  },
  "parser": "vue-eslint-parser",
  "parserOptions": {
    parser: "babel-eslint",
    "ecmaVersion": 2017,
    "sourceType": "module",
    "ecmaFeatures": {
      experimentalObjectRestSpread: true
    }
  },
  extends: [
    '@nuxtjs',
    'prettier',
    'prettier/vue',
    'plugin:prettier/recommended',
    "plugin:vue/vue3-recommended",
    'plugin:nuxt/recommended'
  ],
  plugins: [
    'prettier',
    'vue'
  ],
  // add your custom rules here
  rules: {

      // we have to load & include prettierconfig here to make IDE's respect the rules, otherwise the prettier rules are not recognized sometimes.
      "prettier/prettier": ["warn", prettierConfig, {
        "usePrettierrc": true
      }],

    'nuxt/no-cjs-in-config': 'off',
    'no-unused-vars': 'off',
    'standard/computed-property-even-spacing' : 'off',
    'unicorn/number-literal-case' : 'off',
    'no-console' : 'off'
  }
}
