import { storiesOf } from '@storybook/vue'
import Edit from '~/components/Views/Edit/Edit'

storiesOf('Views', module).add(
  'edit',
  () => ({
    components: { Edit },
    template: `
      <edit/>`
  }),
  {
    info: `Edit view`
  }
)
