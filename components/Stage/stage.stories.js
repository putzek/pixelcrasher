import { storiesOf } from '@storybook/vue'
import Stage from './Stage'

storiesOf('Stage', module).add(
  'main stage',
  () => ({
    components: { Stage },
    template: `
      <div style="width: 100vw; height: 100vh;">
        <stage></stage>
      </div>`
  }),
  {
    info: `The stage element`
  }
)
