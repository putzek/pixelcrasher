import { registerComponent } from 'aframe'
import * as THREE from 'three'
import { getStripe } from './helpers'
registerComponent('a-stripe', {
  schema: {
    ledCount: { type: 'int', default: 144 },
    ledsPerMeter: { type: 'int', default: 144 },
    id: { type: 'string' }
  },
  init() {
    this.el.frameData = []
    this.length = this.data.ledCount * (1 / this.data.ledsPerMeter)
    this.stripe = this.buildView()
    this.updateView(this.el.frameData)
  },
  update() {},
  tick() {
    // this.updateView(this.el.frameData)
  },
  remove() {},
  pause() {},
  play() {},
  buildView() {
    const stripe = getStripe({ color: '#fff', size: this.length })
    this.el.object3D.add(stripe)
    return stripe
  },
  updateView(newData = []) {
    // set colors from newData
    if (newData.length > this.data.ledCount * 3) {
      console.error(
        `Wrong frame size: Wanted ${this.data.ledCount} but got ${newData.length}`
      )
      return false
    }
    // create a texture out of it
    const texture = new THREE.DataTexture(
      newData,
      this.data.ledCount,
      1,
      THREE.RGBFormat
    )
    // apply texture to front, top and bottom side
    this.stripe.material[2].map = texture
    this.stripe.material[3].map = texture
    this.stripe.material[4].map = texture
    // delete texture (will never be reused)
    texture.dispose()
  }
})
