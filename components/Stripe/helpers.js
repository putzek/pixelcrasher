import * as THREE from 'three'
export const getStripe = ({ color, size }) => {
  const blackMaterial = new THREE.MeshBasicMaterial({ color: '#000' })
  const geometry = new THREE.BoxGeometry(size, 4 / 100, 4 / 100)
  // create inital material, the sides which contain leds will be updated for every frame
  const materials = [
    new THREE.MeshBasicMaterial({
      color: '#222',
      transparent: true
    }),
    new THREE.MeshBasicMaterial({
      color: '#222',
      transparent: true
    }),
    new THREE.MeshBasicMaterial({
      map: blackMaterial,
      side: THREE.FrontSide,
      precision: 'lowp'
    }),
    new THREE.MeshBasicMaterial({
      map: blackMaterial,
      side: THREE.FrontSide,
      precision: 'lowp'
    }),
    // front
    new THREE.MeshBasicMaterial({
      map: blackMaterial,
      side: THREE.FrontSide,
      precision: 'lowp'
    }),
    // back
    new THREE.MeshBasicMaterial({ color: '#111', precision: 'lowp' })
  ]
  // generate Mesh
  const stripe = new THREE.Mesh(geometry, materials)
  stripe.position.set(size / 2, 2 / 100, 2 / 100)
  return stripe
}
