/**
 * This file configures the webpack config to use for storybook.
 * The default storybook config is extended because we need additional functionality.
 * @see https://storybook.js.org/docs/configurations/custom-webpack-config/
 * @param config
 * @param mode
 * @returns {Promise<*>}
 */
const path = require("path")
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const mode = "DEVELOPMENT"

module.exports = async ({ config, mode }) => {
  // Add docgen loader; allows vue props to be shown in doc tab in storybook
  const vueDocgenRule = {
    test: /\.vue$/,
    loader: "vue-docgen-loader",
    enforce: "post"
  };

  // add loader for sass to allow importing main scss file from project
  config.module.rules = [
    ...config.module.rules,
    {
      test: /\.(scss)$/,
      resolve: { extensions: [".scss"]},
      use: [
        { loader: "vue-style-loader"},
        { loader: "css-loader"},
       /* {
          loader: 'resolve-url-loader',
          options: {
            debug: true
          }
        },*/
        'sass-loader?sourceMap',
      ]
    },
    vueDocgenRule
  ];
  config.plugins = [
    ...config.plugins,
    /**
     * Extract CSS which is included in javascript files
     * @see javascript/index.js
     */
    new MiniCssExtractPlugin({
      chunkFilename:
        mode === "DEVELOPMENT" ? "[name].css" : "[name].[contenthash:8].css",
      ignoreOrder: false // Enable to remove warnings about conflicting order
    }),
  ];
  config.resolve.alias["@"] =  path.dirname(path.resolve(__dirname))
  config.resolve.alias["~"] =  path.dirname(path.resolve(__dirname))
  config.resolve.modules = [
    ...config.resolve.modules,
    // fix stupid bug which prevented `background-image: url('~assets...') stuff from being loaded
    path.dirname(path.resolve(__dirname))
  ]
  return config;
};

