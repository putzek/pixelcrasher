import { create } from "@storybook/theming/create";
import logo from './logo.jpg'
export default create({
  base: "light",

  colorPrimary: "#0088d0",
  colorSecondary: "#858c94",

  // UI
  appBg: "#efefef",
  appContentBg: "white",
  appBorderColor: "grey",
  appBorderRadius: 4,

  // Typography
  fontBase: '"Open Sans", sans-serif',
  fontCode: "monospace",

  // Text colors
  textColor: "#222",
  textInverseColor: "rgba(255,255,255,0.9)",

  // Toolbar default and active colors
  barTextColor: "#444",
  barSelectedColor: "#0088d0",
  barBg: "#eff0ee",

  // Form colors
  inputBg: "white",
  inputBorder: "silver",
  inputTextColor: "#222",
  inputBorderRadius: 4,

  brandTitle: "Pixelcrasher",
  brandUrl: "https://pixelcrasher.app",
  brandImage: logo
});
