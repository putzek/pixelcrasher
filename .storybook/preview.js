import Vue from 'vue'
import Vuex from 'vuex'
import { configure } from '@storybook/vue'
import { action } from '@storybook/addon-actions'
// add info panel for showing props of vue components in storybook
import { addDecorator } from "@storybook/vue";
import { withInfo } from "storybook-addon-vue-info";

import { setDefaults } from "storybook-addon-vue-info";
setDefaults();
// ignore a-frame and rw components
Vue.config.ignoredElements = [/^a-/, /^rw-/]

addDecorator(withInfo);


// add vue extensions
/*
Use this syntax to add extensions to vue

import VueAccessibleModal from "vue-accessible-modal";

Vue.use(VueAccessibleModal, {
  transition: "fade"
});
*/

Vue.use(Vuex)
Vue.component('nuxt-link', {
  props: ['to'],
  methods: {
    log() {
      action('link target')(this.to)
    },
  },
  template: '<a href="#" @click.prevent="log()"><slot>NuxtLink</slot></a>',
})
function loadStories() {
  const req = require.context('../components', true, /\.stories\.js$/)
  req.keys().forEach(filename => req(filename))
}
configure(loadStories, module)

