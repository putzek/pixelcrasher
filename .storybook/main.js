module.exports = {
  stories: ['../**/*.stories.js'],
  addons: [
  '@storybook/addon-actions',
  '@storybook/addon-links',
  "storybook-addon-vue-info/lib/register",
  "@storybook/addon-docs",
  "@storybook/addon-knobs/register",
  "@storybook/addon-storysource"
]
};

