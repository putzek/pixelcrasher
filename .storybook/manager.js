import { addons } from "@storybook/addons";
import pixelcrasherTheme from "./theme/pixelcrasher-theme";

addons.setConfig({
  theme: pixelcrasherTheme,
  /**
   * where to show the addon panel
   * @type {('bottom'|'right')}
   */
  panelPosition: "right"
});
