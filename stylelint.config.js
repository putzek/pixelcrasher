module.exports = {
  extends: 'stylelint-config-standard',
  rules: {
    'at-rule-empty-line-before': null,
    'selector-type-no-unknown': null
  }
  // add your custom config here
  // https://stylelint.io/user-guide/configuration
}
