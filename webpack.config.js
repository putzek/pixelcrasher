/**
 * This file is not read by nuxt.
 * Its only for IDE's to make them resolve paths properly
 */
const path = require('path')
module.exports = {
  resolve: {
    // for WebStorm
    alias: {
      '@': path.resolve(__dirname),
      '~': path.resolve(__dirname)
    }
  }
}
