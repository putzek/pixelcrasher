const generateStripeData = (length, color, options) => {
  const colorFormatted = color.toRgb()
  const data = new Uint8ClampedArray(length * 3)
  data.fill(0)

  if (options.nightrider.width > 120 || options.nightrider.width < 10) {
    options.nightrider.widthChangePerFrame = -options.nightrider
      .widthChangePerFrame
  }
  options.nightrider.width =
    options.nightrider.width + options.nightrider.widthChangePerFrame

  const endPosition =
    options.nightrider.startPosition + options.nightrider.width

  if (
    options.nightrider.startPosition > length ||
    options.nightrider.startPosition < 0
  ) {
    options.nightrider.stepPerFrame = -options.nightrider.stepPerFrame
  }
  options.nightrider.startPosition =
    options.nightrider.startPosition + options.nightrider.stepPerFrame

  const { r, g, b } = colorFormatted
  for (
    let position = options.nightrider.startPosition * 3;
    Math.max(position, 0) < Math.min(endPosition * 3, length * 3);
    position += 3
  ) {
    if (position < 0 || position >= length * 3) {
      continue
    }
    data.set([r, g, b], position)
  }

  return data
}

class Nightrider {
  constructor(props) {
    this.props = props
    this.options = {
      nightrider: {
        startPosition: 0,
        stepPerFrame: 8,
        width: 10,
        widthChangePerFrame: 1
      }
    }
  }

  generateStripeData() {
    return generateStripeData(this.props.length, this.props.color, this.options)
  }
}

export const get = function(props) {
  return new Nightrider(props)
}
